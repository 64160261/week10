package com.phichet.week10;

public class Circle extends Shape{
    private double raduis;
    public Circle(double raduis){
        super("Circle");
        this.raduis = raduis;
    }
    @Override
    public double calArea() {
        return Math.PI * this.raduis * this.raduis;
    }

    @Override
    public double calPerimeter() {
        return Math.PI * this.raduis;
    }
    @Override
    public String toString() {
        return this.getName() + " raduis:" + this.raduis;
    }

}
