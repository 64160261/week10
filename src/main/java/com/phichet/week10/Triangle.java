package com.phichet.week10;
import java.lang.Math;
public class Triangle extends Shape{
    private double a;
    private double b;
    private double c;
    public Triangle(double a,double b,double c){
        super("Triangle");
        this.a = a;
        this.b = b;
        this.c = c;
    }
    public double getA(){
        return a;
    }
    public double getB(){
        return b;
    }
    public double getC(){
        return c;
    }
    public String toString(){
        return this.getName() + " A: " + this.getA() + " B: " + this.getB() + " C: " + this.getC();
    }
    @Override
    public double calArea() {
        double s = (a + b + c) / 2;
        return Math.sqrt(s*(s-a)*(s-b)*(s-c));
    }
    @Override
    public double calPerimeter() {
        return a + b + c;
    }
}
